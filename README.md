# Process Hook (pshook)

Wrap your binaries with the pshook wrapper enabling process related logging each
time th executable is called. This includes all arguments, CPU info over execution
time, CPU Core utilized and more
